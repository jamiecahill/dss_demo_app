# Disney Streaming Services Demo Application

![Demo Image](https://gitlab.com/jamiecahill/dss_demo_app/-/wikis/uploads/7482c197a403455fe218216a6a732d21/image.png)

Demo application built using [SDL 2.0](https://www.libsdl.org/). Use the arrow keys to navigate around the screen. Use the Enter key to select a show and the Escape or Backspace keys to dismiss the prompt.

## Quick Install (Ubunutu)

On Ubuntu install the necesary runtime dependencies via

```
sudo apt-get install libsdl2-2.0 libsdl2-dev libcurl4-openssl-dev libsdl2-image-dev libsdl2-ttf-dev
```

### Install from Pre-compiled Binary
Pre-compiled binaries are avialable from the [CI pipeline as artifacts](https://gitlab.com/jamiecahill/dss_demo_app/-/pipelines?scope=tags&page=1). 
Unzip the file and run the `dssapp` executable (your working directory must be the same directory as the executable)

### Build From Source

Requirements:
- Make
- Cmake (for spdlog)
- C++17 compatible compiler (tested using gcc 9.3.0)

To build and run use

```
make -j
./dssapp
```

## Tests
Tests are located in the `tests/` directory and can be run via command
```
make test
```
