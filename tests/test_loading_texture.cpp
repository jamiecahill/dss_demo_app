#include "catch_amalgamated.hpp"
#include "SDLWrapper.h"
#include "Endpoints.h"
#include "DSSApp.h"

// These tests will fail in an environment without a GUI - i.e the CI server
// so they are tagged as mayfail but are expected to pass locally

TEST_CASE("Test loading image", "[!mayfail]")
{
    bool success = true;
    auto window = initWindow(1000,1000,"test");
    auto renderer = initRenderer(window.get());

    //Load PNG texture
    auto gTexture = loadTextureFromPath("tests/example.jpg", renderer.get());
    if (gTexture.get() == nullptr)
    {
        printf("Failed to load texture image!\n");
        success = false;
    }

    REQUIRE(success);
}

TEST_CASE("Test loading image from url","[!mayfail]")
{
    const std::string url = "https://prod-ripcut-delivery.disney-plus.net/v1/variant/disney/3C33485A3043C22B8C89E131693E8B5B9306DAA4E48612A655560752977728A6/scale?format=jpeg&quality=90&scalingAlgorithm=lanczos3&width=500";
    bool success = true;
    auto window = initWindow(1000,1000,"test");
    auto renderer = initRenderer(window.get());

    //Load PNG texture
    auto stringPng = httpGetImage(url);
    auto surface =loadSurfaceFromStringPng(stringPng.value());
    auto texture = loadTextureFromSurface(surface.get(), renderer.get());
    if (texture.get() == nullptr)
    {
        printf("Failed to load texture image!\n");
        success = false;
    }
    REQUIRE(success);
}
