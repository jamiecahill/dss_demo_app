#include <catch_amalgamated.hpp>
#include "JSONReader.h"
#include "Endpoints.h"
#include <iostream>
using json = nlohmann::json;

TEST_CASE("Test curling the home screen")
{
    auto response = httpGetHomescreen();
    REQUIRE(response.has_value());
}

TEST_CASE("Test curling and parsing the refsets")
{
    auto response = httpGetHomescreen();
    REQUIRE(response.has_value());
    //auto homescreenjson = json::parse(response.value());
    auto homescreencontent = parseHomeScreen(response.value()  );
    for (auto& refset : homescreencontent.refsets)
    {
        auto refsetresponse = httpGetRefSet(refset.refId);
        REQUIRE(refsetresponse.has_value());
        auto refsetcontent = parseContentSet(refsetresponse.value()["data"]);
    }
}
