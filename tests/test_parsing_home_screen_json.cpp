#include "catch_amalgamated.hpp"
#include "JSONReader.h"
#include <iostream>
#include <fstream> // std::ifstream
#include "json.hpp"

using json = nlohmann::json;

TEST_CASE("Test parsing the homescreen json")
{
    std::ifstream ifs("tests/example_homescreen.json");
    auto exampleData = json::parse(ifs);
    parseHomeScreen(exampleData);
}


TEST_CASE("Test parsing the refset json")
{
    std::ifstream ifs("tests/example_ref.json");
    auto exampleData = json::parse(ifs);
    parseContentSet(exampleData["data"]);
}
