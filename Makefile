CPP = $(wildcard src/*.cpp)
TEST_CPP=$(filter-out src/main.cpp,$(CPP)) $(wildcard tests/*.cpp)
INCLUDES=-I src/ -I include/ -I spdlog/include/
TEST_INCLUDES=-I tests/

OBJ = $(CPP:%.cpp=$(BUILD_DIR)/%.o)
TEST_OBJ =$(TEST_CPP:%.cpp=$(BUILD_DIR)/%.o)
DEP = $(OBJ:%.o=%.d)
TEST_DEP =$(TEST_OBJ:%.o=%.d)

CC = g++

BUILD_DIR = ./build

COMPILER_FLAGS = -Wall -Werror -std=c++17  -g -Wextra

DEFINES = -DJSON_USE_IMPLICIT_CONVERSIONS=0 -DFMT_HEADER_ONLY=1

LINKER_FLAGS = -lSDL2 -lcurl -lSDL2_image -lspdlog -L./spdlog/build -pthread -lSDL2_ttf

TARGET = dssapp
 
TEST_TARGET = catchtest

all : $(TARGET)

$(TARGET) : $(OBJ)
	$(CC) $(OBJ) $(INCLUDES) $(COMPILER_FLAGS) $(LINKER_FLAGS) $(DEFINES) -o $(TARGET)


debug: LINKER_FLAGS += 
debug: CPP+=$(wildcard debug/*.cpp)
debug : $(TARGET)

test : $(TEST_OBJ)
	$(CC) $(INCLUDES)  $(TEST_INCLUDES)  $(TEST_OBJ) $(COMPILER_FLAGS) $(LINKER_FLAGS) $(DEFINES) -o $(TEST_TARGET)
	./$(TEST_TARGET)

-include $(DEP)

# Downloads and compiles the spdlog C++ logging library
$(BUILD_DIR)/%.o : %.cpp spdlog/build/libspdlog.a
	mkdir -p $(@D)
	$(CC) $(COMPILER_FLAGS) $(INCLUDES) $(TEST_INCLUDES) -MMD -c $< -o $@

spdlog/build/libspdlog.a:
	git clone https://github.com/gabime/spdlog.git || true
	cd spdlog && mkdir build -p && cd build
	cd spdlog/build && cmake .. && make -j

.PHONY : clean format

clean :
	rm -rf $(BUILD_DIR) $(OBJ_NAME)
	rm $(TARGET) || true
#rm -rf spdlog


format : 
	clang-format-11 -i src/*.cpp
	clang-format-11 -i src/*.h
