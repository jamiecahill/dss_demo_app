#pragma once
#include "Colors.h"
#include "LRUCache.h"
#include "SDLWrapper.h"
#include "spdlog/spdlog.h"
#include <SDL2/SDL_ttf.h>
#include <memory>

class TextRenderer {

public:
  enum TextAdjust { Left, Center, Right };
  enum Font { SansProRegular };

  TextRenderer(int cacheSize = 100) : cache(cacheSize){};

  /// Render the text to the specified x,y location at the specified size
  ///  The text will grow to the right indefinitely based on the length of the
  ///  string
  void renderText(const std::string &text, SDL_Renderer *renderer, int xPos,
                  int yPos, int fontSize, Font font = SansProRegular,
                  SDL_Color color = Colors::White);

  /// Render on one line within the specified rectangle. Will shrink the size of
  /// the text down the fit the
  ///  size of the rectangle if need be
  void renderTextInRect(const std::string &text, SDL_Renderer *renderer,
                        SDL_Rect rect, TextAdjust textAdjust,
                        std::optional<int> maxFontSize,
                        Font font = SansProRegular,
                        SDL_Color color = Colors::White);

private:
  cache::lru_cache<std::pair<const std::string, int>, std::shared_ptr<TTF_Font>>
      cache;
};

/// Return the location of the font file for the given font
std::string getFontFile(TextRenderer::Font font);
