
#include "Endpoints.h"
#include <curl/curl.h>
#include <iostream>
#include <optional>
#include <stdio.h>
#include <string>

using json = nlohmann::json;

// https://cd-static.bamgrid.com/dp-117731241344/home.json will provide data to
// populate a “Home” page similar to the current Disney+ experience.
// - https://cd-static.bamgrid.com/dp-117731241344/sets/<ref id>.json will
// provide data for dynamic “ref” sets. The “ref id” will be provided in the
// “home.json”

const std::string HOME_SCREEN_API_ENDPOINT =
    "https://cd-static.bamgrid.com/dp-117731241344/home.json";

std::string createRefSetURL(const std::string &refId) {
  return "https://cd-static.bamgrid.com/dp-117731241344/sets/" + refId +
         ".json";
}

size_t writeFunction(void *ptr, size_t size, size_t nmemb, std::string *data) {
  data->append((char *)ptr, size * nmemb);
  return size * nmemb;
}

std::pair<long, std::string> do_curl(const std::string &endpoint) {
  // From https://gist.github.com/whoshuu/2dc858b8730079602044
  auto curl = curl_easy_init();
  if (curl) {
    curl_easy_setopt(curl, CURLOPT_URL, endpoint.c_str());
    curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 1L);
    curl_easy_setopt(curl, CURLOPT_USERPWD, "user:pass");
    curl_easy_setopt(curl, CURLOPT_USERAGENT, "curl/7.42.0");
    curl_easy_setopt(curl, CURLOPT_MAXREDIRS, 50L);
    curl_easy_setopt(curl, CURLOPT_TCP_KEEPALIVE, 1L);

    std::string response_string;
    std::string header_string;
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writeFunction);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &response_string);
    curl_easy_setopt(curl, CURLOPT_HEADERDATA, &header_string);

    char *url;
    long response_code = -1;
    double elapsed;
    curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &response_code);
    curl_easy_getinfo(curl, CURLINFO_TOTAL_TIME, &elapsed);
    curl_easy_getinfo(curl, CURLINFO_EFFECTIVE_URL, &url);

    curl_easy_perform(curl);
    curl_easy_cleanup(curl);
    curl = NULL;
    return std::make_pair(response_code, response_string);
  }
  return std::make_pair(-1, "");
}

std::optional<json> httpGetHomescreen(void) {
  std::optional<json> jsonResponse;
  long responseCode;
  std::string response;
  std::tie(responseCode, response) = do_curl(HOME_SCREEN_API_ENDPOINT);
  if (responseCode == 0) {
    jsonResponse = json::parse(response);
  }
  return jsonResponse;
}

std::optional<std::string> httpGetImage(const std::string &imageURL) {
  long responseCode;
  std::string response;
  std::tie(responseCode, response) = do_curl(imageURL);
  if (responseCode == 0) {
    return std::make_optional(response);
  }
  return std::nullopt;
}

std::optional<json> httpGetRefSet(const std::string &refId) {
  std::optional<json> jsonResponse;
  long responseCode;
  std::string response;
  std::tie(responseCode, response) = do_curl(createRefSetURL(refId));
  if (responseCode == 0) {
    jsonResponse = json::parse(response);
  }
  return jsonResponse;
}
