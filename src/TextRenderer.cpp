#include "TextRenderer.h"

const std::string FONT_FILE = "source-sans-pro/SourceSansPro-Regular.ttf";

/// TODO: Use path relative to executable and not current working directory
std::string getFontFile(TextRenderer::Font font) {
  switch (font) {
  case TextRenderer::Font::SansProRegular:
    return FONT_FILE;
  default:
    spdlog::error("Could not find font {}", font);
    return "";
  }
}

void TextRenderer::renderText(const std::string &text, SDL_Renderer *renderer,
                              int xPos, int yPos, int fontSize,
                              TextRenderer::Font font, SDL_Color color) {

  auto fontSettings = std::make_pair(getFontFile(font), fontSize);

  // If the font has not already been created create it
  if (!cache.exists(fontSettings)) {
    cache.put(fontSettings,
              make_sdl_shared(TTF_OpenFont(fontSettings.first.c_str(),
                                           fontSettings.second)));
  }
  auto sans = cache.get(fontSettings).get();

  SDLPtr<SDL_Surface> surfaceMessage(
      TTF_RenderText_Solid(sans, text.c_str(), color));

  SDLPtr<SDL_Texture> texture(
      SDL_CreateTextureFromSurface(renderer, surfaceMessage.get()));
  SDL_Rect rect = {xPos, yPos, surfaceMessage->w, surfaceMessage->h};
  SDL_RenderCopy(renderer, texture.get(), NULL, &rect);
}

void TextRenderer::renderTextInRect(const std::string &text,
                                    SDL_Renderer *renderer, SDL_Rect rect,
                                    TextAdjust textAdjust,
                                    std::optional<int> maxFontSize,
                                    TextRenderer::Font font, SDL_Color color) {
  if (text.size() == 0)
    return;

  int fontSize = rect.h;
  if (maxFontSize)
    fontSize = std::min(maxFontSize.value(), fontSize);

  int w = rect.w + 1;
  int h;
  TTF_Font *sans = nullptr;

  // Shrink the font down until it fits within the provided rectangle
  do {
    auto fontSettings = std::make_pair(getFontFile(font), fontSize--);
    if (!cache.exists(fontSettings)) {
      cache.put(fontSettings,
                make_sdl_shared(TTF_OpenFont(fontSettings.first.c_str(),
                                             fontSettings.second)));
    }
    sans = cache.get(fontSettings).get();

    if (TTF_SizeText(sans, text.c_str(), &w, &h)) {
      spdlog::error("Could not render text {}", text);
      return;
    }
  } while (w > rect.w);

  // Center the y position font within the rectangle
  rect.y += (rect.h - h) / 2;

  switch (textAdjust) {
  case TextAdjust::Center:
    rect.x += (rect.w - w) / 2;
    break;
  case TextAdjust::Right:
    rect.x += (rect.w - w);
    break;
  case TextAdjust::Left:
    // do nothing - text is already left adjusted
    break;
  }

  SDLPtr<SDL_Surface> surfaceMessage(
      TTF_RenderText_Solid(sans, text.c_str(), color));

  SDLPtr<SDL_Texture> texture(
      SDL_CreateTextureFromSurface(renderer, surfaceMessage.get()));
  rect.w = surfaceMessage->w;
  rect.h = surfaceMessage->h;
  SDL_RenderCopy(renderer, texture.get(), NULL, &rect);
}
