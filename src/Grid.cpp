#include "Grid.h"
#include "Constants.h"

int tileMaxHeight(int tileWidth) {
  return tileWidth * TILE_ASPECT_RATIO * SELECTED_TILE_SIZE_RATIO;
}

/// Create an SDL_Rect that encompasses the provided rectangle
SDL_Rect createHighlightRectangle(const SDL_Rect &rect, int borderSize) {
  SDL_Rect r;
  r.x = rect.x - borderSize;
  r.y = rect.y - borderSize;
  r.w = rect.w + (borderSize * 2);
  r.h = rect.h + (borderSize * 2);
  return r;
}

/// Construct a tile
///  x - x position of upper lefthand corner
///  y - y position of upper lefthand corner
///  tilewidth - width in pixels of the tile when unselected
Tile::Tile(int x, int y, int tileWidth)
    : tileWidth(tileWidth), state(TileState::NOTSELECTED) {
  rect.x = x;
  rect.y = y;
  rect.w = tileWidth;
  rect.h = rect.w * TILE_ASPECT_RATIO;
}

void Tile::update(int frames, int x, int y) {
  rect.x = x;
  rect.y = y;
  switch (state) {
  case SELECTED:
    if (rect.w < maxWidth()) {
      rect.w += growthRate(frames);
    }
    break;
  case NOTSELECTED:
    if (rect.w > tileWidth) {
      rect.w -= growthRate(frames);
    }
    break;
  }
  rect.h = rect.w * TILE_ASPECT_RATIO;
}

void Tile::select() { state = TileState::SELECTED; }
void Tile::deselect() { state = TileState::NOTSELECTED; }
TileState Tile::getState() const { return state; }

/// Returns the current width of the tile
int Tile::width() const { return rect.w; }

/// Returns the maximum width the tile could be when selected
int Tile::maxHeight() const { return tileMaxHeight(tileWidth); }

/// Returns the maximum height the tile could be when selected
int Tile::maxWidth() const { return tileWidth * SELECTED_TILE_SIZE_RATIO; }

const SDL_Rect &Tile::getRectangle() const { return rect; }

/// Returns growth/shrink rate of the tile during the selection/deselection
/// transition in pixels per frame
int Tile::growthRate(int frames) const {
  return std::ceil(maxWidth() / (DSSConstants::MaxFPS / 2)) * frames;
}

/// Compute the number of tiles hidden to the left of the screen
///  based off the provided row offset, activeColumn index, and number of
///  tiles of the row that can displayed on screen at once
int computeNumberOfTilesOffScreen(int offset, int activeCol,
                                  int maxTilesDisplayed) {
  if (activeCol - offset < 0) {
    return std::max(activeCol, 0);
  } else if (activeCol - offset > maxTilesDisplayed - 1) {
    return std::max(activeCol - maxTilesDisplayed + 1, 0);
  }
  return std::max(offset, 0);
}

Row::Row(int x, int y, int tileWidth, int horizontalSpan, int verticalSpan,
         int numTiles, int rowWidth)
    : initialX(x), initialY(y), rowX(x), rowY(y),
      horizontalSpan(horizontalSpan), verticalSpan(verticalSpan),
      tileWidth(tileWidth), numTiles(numTiles), offset(0), activeCol(0),
      expectedX(x), rowWidth(rowWidth) {
  maxTilesDisplayed = floor(static_cast<float>(rowWidth) /
                            static_cast<float>((tileWidth + horizontalSpan)));

  for (int i = 0; i < numTiles; i++) {
    tiles.push_back(Tile(x, y, tileWidth));
    x += tileWidth + horizontalSpan;
  }
}

void Row::deselectAllTiles() {
  for (auto &tile : tiles) {
    tile.deselect();
  }
}

/// Set the active column (and tile) of the row
///  relative to the columns on view of the  screen
/// relativeCol - int between 0 and (maxTilesDisplayed-1)
///    where 0 sets the active column to be the left most
///    column of the row on screen and (maxTilesDisplayed-1)
///    sets the active column to be the right most column of
///    the row on screen
void Row::setActiveColumnFromRelativeCol(int relativeCol) {
  deselectAllTiles();
  activeCol = relativeCol + offset;
  tiles[activeCol].select();
}

/// Get the index of the active column relative to the columns
///  on view of the screen. Returns an int between 0 and
///  (maxTilesDisplayed-1) where 0 means the active column is the
///  leftmost column on screen and (maxTilesDisplayed-1) means
///  the active column is the rightmost column on screen
int Row::getActiveRelativeCol() const { return activeCol - offset; }

/// Returns the bottom y position of the row
int Row::bottomY() const { return rowY + rowHeight(); }

/// Returns the height of the row in pixels
int Row::rowHeight() const { return tiles.front().maxHeight() + verticalSpan; }

/// Update the state of the row forward frames in time
///  updating the tiles and their positions
void Row::update(int newY, int frames) {
  int growthRate = std::ceil(rowWidth / (DSSConstants::MaxFPS / 1.5)) * frames;

  rowY = newY;
  if (rowX > expectedX) {
    rowX -= growthRate;
    rowX = std::max(rowX, expectedX);
  } else if (rowX < expectedX) {
    rowX += growthRate;
    rowX = std::min(rowX, expectedX);
  }
  int x = rowX;
  for (auto &tile : tiles) {
    tile.update(frames, x, newY);
    x += tile.width() + horizontalSpan;
  }
}

/// Set the column/tile left of the currently active tile
///  as the new active column/tile
void Row::moveLeft() {
  deselectAllTiles();
  activeCol = std::max(activeCol - 1, 0);
  tiles[activeCol].select();
  offset = computeNumberOfTilesOffScreen(offset, activeCol, maxTilesDisplayed);
  expectedX = -offset * (tileWidth + horizontalSpan) + initialX;
}

/// Set the column/tile right of the currently active tile
///  as the new active column/tile
void Row::moveRight() {
  deselectAllTiles();
  activeCol = std::min(activeCol + 1, numTiles - 1);
  offset = computeNumberOfTilesOffScreen(offset, activeCol, maxTilesDisplayed);
  tiles[activeCol].select();
  expectedX = -offset * (tileWidth + horizontalSpan) + initialX;
}

/// Return the active tile if the active tile
///  is in this row, std::nullopt otherwise
std::optional<Tile> Row::getActiveTile() const {
  for (auto &tile : tiles) {
    if (tile.getState() == SELECTED) {
      return std::make_optional(tile);
    }
  }
  return std::nullopt;
}

/// Return the active column of the row
int Row::getActiveCol() const { return activeCol; }

const std::vector<Tile> &Row::getTiles() const { return tiles; }
std::vector<Tile> &Row::getTiles() { return tiles; }

std::ostream &operator<<(std::ostream &os, const Row &r) {
  os << "Row(activeCol=" << r.getActiveCol() << " offset=" << r.offset << ")";
  return os;
}

Grid::Grid(int x, int y, int tileWidth, int horizontalSpan, int verticalSpan,
           int gridWidth, int gridHeight, std::vector<int> sizePerRow)
    : activeRow(0), offset(0), currentY(y), initialX(x), initialY(y),
      maxRowsDisplayed(0), horizontalSpan(horizontalSpan),
      verticalSpan(verticalSpan), expectedY(y), tileWidth(tileWidth),
      gridWidth(gridWidth), gridHeight(gridHeight) {
  for (size_t i = 0; i < sizePerRow.size(); i++) {
    rows.push_back(Row(x, y, tileWidth, horizontalSpan, verticalSpan,
                       sizePerRow[i], gridWidth));
    y += rows.front().rowHeight();
    if (y <= gridHeight) {
      maxRowsDisplayed++;
    }
  }
}

/// Returns the height in pixels of a row within the grid
int Grid::rowHeight() { return rows.size() ? rows.front().rowHeight() : 0; }

/// Move the cursor left by one tile
void Grid::moveLeft() { rows[activeRow].moveLeft(); }

/// Move the cursor right by one tile
void Grid::moveRight() { rows[activeRow].moveRight(); }

/// Deselect all active tiles
void Grid::deSelectAllTiles() {
  for (auto &row : rows) {
    for (auto &tile : row.getTiles())
      tile.deselect();
  }
}

/// Move the cursor up one row of tiles
void Grid::moveUp() {
  deSelectAllTiles();
  auto currentActiveRelativeCol = rows[activeRow].getActiveRelativeCol();
  activeRow = std::max(activeRow - 1, 0);
  rows[activeRow].setActiveColumnFromRelativeCol(currentActiveRelativeCol);
  offset = computeNumberOfTilesOffScreen(offset, activeRow, maxRowsDisplayed);
  expectedY = -offset * rowHeight() + initialY;
}

/// Move the cursor up one row of tiles
void Grid::moveDown() {
  deSelectAllTiles();
  auto currentActiveRelativeCol = rows[activeRow].getActiveRelativeCol();
  activeRow = std::min(activeRow + 1, static_cast<int>(rows.size() - 1));
  rows[activeRow].setActiveColumnFromRelativeCol(currentActiveRelativeCol);
  offset = computeNumberOfTilesOffScreen(offset, activeRow, maxRowsDisplayed);
  expectedY = -offset * rowHeight() + initialY;
}

/// Move the grid forward in time updating the elements of the grid to reflect
/// the current state
///  of the grid and playing any animations
void Grid::update(int frames) {
  int growthRate = std::ceil(gridHeight / (DSSConstants::MaxFPS / 2)) * frames;
  if (currentY > expectedY) {
    currentY -= growthRate;
    currentY = std::max(currentY, expectedY);
  } else if (currentY < expectedY) {
    currentY += growthRate;
    currentY = std::min(currentY, expectedY);
  }
  int y = currentY;
  for (auto &row : rows) {
    row.update(y, frames);
    y += row.rowHeight();
  }
}

/// Return the active tile if there is an active tile within the grid,
/// std::nullopt otherwise
std::optional<Tile> Grid::getActiveTile() const {
  for (auto &row : rows) {
    auto activeTile = row.getActiveTile();
    if (activeTile) {
      return activeTile;
    }
  }
  return std::nullopt;
}

const std::vector<Row> &Grid::getRows() const { return rows; }

std::ostream &operator<<(std::ostream &os, const Grid &grid) {
  os << "Grid(activeRow=" << grid.activeRow << " " << grid.rows[grid.activeRow]
     << ")";
  return os;
}

int Grid::getMaximumTilesDisplayed() {
  return (maxRowsDisplayed + 1) *
         ((rows.size() ? rows.front().getTiles().size() : 1) + 1);
}

void Grid::addRow(int numtiles) {
  auto y = rows.size() ? rows.back().bottomY() : initialY;
  rows.push_back(Row(initialX, y, tileWidth, horizontalSpan, verticalSpan,
                     numtiles, gridWidth));
}

SDL_Rect Grid::getRowTitleRect(int rowIdx) const {
  auto &row = rows[rowIdx];
  SDL_Rect r;
  r.x = initialX;
  r.y = row.topY() - verticalSpan + HIGHLIGHT_BORDER_SIZE;
  r.w = 0;
  r.h = verticalSpan - HIGHLIGHT_BORDER_SIZE * 5;
  return r;
}
