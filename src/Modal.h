#pragma once

#include "SDLWrapper.h"

// Vertical images are 500 x 704 pixels
const double VERT_CONTENT_TILE_ASPECT_RATIO = (500.0 / 704.0);

// Modal for displaying information about a single piece of content
class Modal {
public:
  enum ActiveButton { BUTTON1, BUTTON2 };

  Modal(int screenWidth, int screenHeight);

  void reset() { activeButton = ActiveButton::BUTTON1; }

  // The move functions handle user input
  //  when the modal is active
  void moveLeft() {
    switch (activeButton) {
    case BUTTON1:
      break;
    case BUTTON2:
      activeButton = BUTTON1;
      break;
    }
  }

  void moveRight() {
    switch (activeButton) {
    case BUTTON1:
      activeButton = BUTTON2;
      break;
    case BUTTON2:
      break;
    }
  }

  void moveUp() {}

  void moveDown() {}

  SDL_Rect getActiveButtonRect() const {
    switch (activeButton) {
    case BUTTON1:
      return leftButton;
    case BUTTON2:
      return rightButton;
      break;
    }
    return {0, 0, 0, 0};
  }

  // Rectangle where the title of the content should be displayed
  SDL_Rect titleRect;
  // Rectangle where the vertical image tile for the content should be displayed
  SDL_Rect vertContentTile;
  // Defines the entire area of the modal
  SDL_Rect modalRect;
  // Rectangle where the rating of the content should be displayed
  SDL_Rect ratingRect;
  // Rectangle where the release of the content should be displayed
  SDL_Rect releaseDateRect;

  // Buttons of the modal
  SDL_Rect leftButton;
  SDL_Rect rightButton;

  // Currently selected button within the modal
  ActiveButton activeButton;
};
