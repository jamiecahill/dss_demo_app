#include "SDLWrapper.h"
#include "spdlog/spdlog.h"
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <fstream>
#include <iostream>
#include <stdio.h>
#include <string>

SDLGlobals::SDLGlobals() {
  if (SDL_Init(SDL_INIT_VIDEO) < 0) {
    printf("SDL could not initialize! SDL Error: %s\n", SDL_GetError());
    status = false;
  }

  // Set texture filtering to linear
  if (!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1")) {
    printf("Warning: Linear texture filtering not enabled!");
  }

  // Initialize SDL_ttf
  if (TTF_Init() == -1) {
    printf("SDL_ttf could not initialize! SDL_ttf Error: %s\n", TTF_GetError());
    status = false;
  }
}

SDLGlobals::~SDLGlobals() {
  // Quit SDL subsystems
  IMG_Quit();
  SDL_Quit();
  TTF_Quit();
}

SDLPtr<SDL_Window> initWindow(int screenWidth, int screenHeight,
                              const std::string &windowName) {
  SDLPtr<SDL_Window> window;

  // Create window
  window.reset(SDL_CreateWindow(windowName.c_str(), SDL_WINDOWPOS_UNDEFINED,
                                SDL_WINDOWPOS_UNDEFINED, screenWidth,
                                screenHeight, SDL_WINDOW_SHOWN));
  if (window.get() == nullptr) {
    spdlog::error("Unable to create SDL Window! SDL Error: {}", SDL_GetError());
  }
  return window;
}

SDLPtr<SDL_Renderer> initRenderer(SDL_Window *window) {
  SDLPtr<SDL_Renderer> renderer(
      SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED));
  if (renderer.get() == nullptr) {
    spdlog::error("Unable to create SDL Renderer! SDL Error: {}",
                  SDL_GetError());
  } else {
    // Initialize renderer color
    SDL_SetRenderDrawColor(renderer.get(), 0xFF, 0xFF, 0xFF, 0xFF);

    // Initialize PNG loading
    int imgFlags = IMG_INIT_PNG;
    if (!(IMG_Init(imgFlags) & imgFlags)) {
      spdlog::error("Unable initialize SDL image! SDL Error: {}",
                    SDL_GetError());
    }
  }
  return renderer;
}

// Loads in data from a stringified png file (like format you get from an http
// get request for an image)
SDLSharedPtr<SDL_Texture> loadTextureFromStringPng(std::string stringPNG,
                                                   SDL_Renderer *renderer) {

  // There is absolutely a better way to load a png/jpeg in from memory
  //  but I cannot convince SDL to create a surface from
  //  a c-string buffer. For now dump the image to a tempfile, read it in, then
  //  delete the tempfile

  // Create a temp file and dump the image to it from the string
  std::string tmpname = std::tmpnam(nullptr);
  std::ofstream out(tmpname);
  out << stringPNG;
  out.close();

  // Load the image as a texture from the tempfile
  auto texture = loadTextureFromPath(tmpname, renderer);

  // Clear the tempfile
  if (remove(tmpname.data()) != 0)
    spdlog::error("Unable to delete tmpfile {}", tmpname);
  return texture;

  // Broken code for trying to create a surface from a c-string
  /*
  FILE* memfile = fmemopen(NULL, sizeof(stringPNG.data())-1, "w+");
  auto written = std::fwrite(stringPNG.data(), sizeof(char),
sizeof(stringPNG.data())-1, memfile); assert(written  ==
sizeof(stringPNG.data())-1 && "Failed to write to memory mapped file");
  std::rewind(memfile);
  SDL_RWops *rw = SDL_RWFromFP(memfile, SDL_TRUE);

  // SDL_RWops *rw
=SDL_RWFromConstMem(stringPNG.data(),sizeof(stringPNG.data()-1) ); std::cerr <<
rw << "\n"; assert(rw  != nullptr && "Failed to write to memory mapped file");
  SDLPtr<SDL_Surface> loadedSurface(IMG_LoadTyped_RW(rw, 1,"JPG"));
SDLPtr<SDL_Texture> newTexture;
if (loadedSurface == NULL) {
  spdlog::error("Unable to lead image from memory {}", IMG_GetError());
} else {
  // Create texture from surface pixels
  newTexture.reset(
      SDL_CreateTextureFromSurface(renderer, loadedSurface.get()));
  if (newTexture == NULL) {
  spdlog::error("Unable to lead image from memory");
  }
}

return newTexture;
*/
}

// Loads in data from a stringified png file (like format you get from an http
// get request for an image)
SDLSharedPtr<SDL_Surface> loadSurfaceFromStringPng(std::string stringPNG) {

  // There is absolutely a better way to load a png/jpeg in from memory
  //  but I cannot convince SDL to create a surface from
  //  a c-string buffer. For now dump the image to a tempfile, read it in, then
  //  delete the tempfile

  std::string tmpname = std::tmpnam(nullptr);
  std::ofstream out(tmpname);
  out << stringPNG;
  out.close();
  auto surface = loadSurfaceFromPath(tmpname);
  if (remove(tmpname.data()) != 0)
    spdlog::error("Unable to delete tmpfile {}", tmpname);
  return surface;
}

SDLSharedPtr<SDL_Surface> loadSurfaceFromPath(std::string path) {
  SDLSharedPtr<SDL_Surface> surface = make_sdl_shared(IMG_Load(path.c_str()));
  if (surface.get() == NULL) {
    spdlog::error("Unable to lead image from memory");
  }
  return surface;
}

SDLSharedPtr<SDL_Texture> loadTextureFromSurface(SDL_Surface *surface,
                                                 SDL_Renderer *renderer) {
  auto newTexture =
      make_sdl_shared(SDL_CreateTextureFromSurface(renderer, surface));
  if (newTexture.get() == NULL) {

    spdlog::error("Unable to create texture from surface! SDL Error: {}",
                  SDL_GetError());
    return SDLSharedPtr<SDL_Texture>(nullptr);
  }
  return newTexture;
}

SDLSharedPtr<SDL_Texture> loadTextureFromPath(std::string path,
                                              SDL_Renderer *renderer) {
  // Load image at specified path
  SDLSharedPtr<SDL_Texture> newTexture(nullptr);

  SDLPtr<SDL_Surface> loadedSurface(IMG_Load(path.c_str()));
  if (loadedSurface == NULL) {
    spdlog::error("Unable to lead image from memory");
  } else {
    // Create texture from surface pixels
    newTexture = make_sdl_shared(
        SDL_CreateTextureFromSurface(renderer, loadedSurface.get()));
    if (newTexture == nullptr) {
      spdlog::error("Unable to create texture from {}s! SDL Error: {}",
                    path.c_str(), SDL_GetError());
    }
  }

  spdlog::info("Loaded image from file {}", path);
  return newTexture;
}

std::ostream &operator<<(std::ostream &os, const SDL_Rect &r) {
  os << "SDLRect(x=" << r.x << ",y=" << r.y << ",h=" << r.h << ",w=" << r.w
     << ")";
  return os;
}
