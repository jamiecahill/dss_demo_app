#include "DSSApp.h"
#include "Colors.h"
#include "Grid.h"
#include "Modal.h"
#include "RefLoader.h"
#include "SDLWrapper.h"
#include "TextRenderer.h"
#include "spdlog/spdlog.h"
#include <Constants.h>
#include <MediaLoader.h>
#include <SDL2/SDL_ttf.h>
#include <chrono>
#include <iostream>
#include <limits>
#include <math.h>

const std::string AppName = "DSS Demo App";

// Screen dimension constants
const int SCREEN_WIDTH = 640 * 2;
const int SCREEN_HEIGHT = 480 * 2;

//  Utility for calling SDL_SetRenderDrawColor with a SDL_Color object
auto SDL_SetRenderDrawColor(SDL_Renderer *renderer, SDL_Color color) {
  return SDL_SetRenderDrawColor(renderer, color.r, color.g, color.b, color.a);
}

// Render the conent within homeScreenContent onto the screen based off the
// positions contained within grid
void renderGrid(const Grid &grid, const HomeScreenContent &homeScreenContent,
                MediaLoader &mediaLoader, SDL_Renderer *renderer,
                TextRenderer &textRenderer,
                const SDL_Rect &prefetchTileRectangle,
                const SDL_Rect &screenRectangle) {
  // Draw the highlight rectangle around the currently selected tile
  auto activeTile = grid.getActiveTile();
  if (activeTile) {
    auto highlightRectangle =
        createHighlightRectangle(activeTile.value().getRectangle());
    SDL_SetRenderDrawColor(renderer, Colors::White);
    SDL_RenderFillRect(renderer, &highlightRectangle);
  }

  int rowIdx = 0;
  // For every content set currently loaded
  for (const auto &row : grid.getRows()) {
    int colIdx = 0;
    // Render the title over th row
    SDL_Rect rowTitleRect = grid.getRowTitleRect(rowIdx);
    std::string title = homeScreenContent.getCollectionTitle(rowIdx);
    textRenderer.renderText(title, renderer, rowTitleRect.x, rowTitleRect.y,
                            rowTitleRect.h);

    // Render each tile in the row
    for (const auto &tile : row.getTiles()) {
      const auto &tileRectangle = tile.getRectangle();
      // Only fetch the textures for tiles on screen
      // (or just offscreen so they get loaded into the cache for faster display
      // if the user moves the screen)
      if (!SDL_HasIntersection(&tileRectangle, &prefetchTileRectangle)) {
        colIdx++;
        continue;
      }

      const auto tileContent = homeScreenContent.getTileContent(rowIdx, colIdx);
      const auto gTexture = mediaLoader.loadTextureFromURL(tileContent.tileURL);

      // Don't render tiles offscreen
      if (SDL_HasIntersection(&tileRectangle, &screenRectangle)) {
        if (gTexture != nullptr) {
          SDL_RenderCopy(renderer, gTexture.get(), NULL, &tileRectangle);
        } else {
          SDL_SetRenderDrawColor(renderer, Colors::BlankTileColor);
          SDL_RenderFillRect(renderer, &tileRectangle);
        }
      }
      colIdx++;
    }
    rowIdx++;
  }
}

void renderModal(const Modal &modal, const Content &modalContent,
                 SDL_Renderer *renderer, TextRenderer &textRenderer,
                 SDL_Texture *modalImageTexture) {
  // Draw the background of the modal
  SDL_SetRenderDrawColor(renderer, Colors::ModalBackgroundColor);
  SDL_RenderFillRect(renderer, &modal.modalRect);

  if (modalImageTexture != nullptr) {
    SDL_RenderCopy(renderer, modalImageTexture, NULL, &modal.vertContentTile);
  } else {
    SDL_SetRenderDrawColor(renderer, Colors::BlankTileColor);
    SDL_RenderFillRect(renderer, &modal.vertContentTile);
  }

  // Render the title of the show
  textRenderer.renderTextInRect(modalContent.title, renderer, modal.titleRect,
                                TextRenderer::TextAdjust::Center, std::nullopt);

  // Render the rating of the show
  textRenderer.renderTextInRect(
      modalContent.rating.size() ? "Rating: " + modalContent.rating : "",
      renderer, modal.ratingRect, TextRenderer::TextAdjust::Right,
      std::nullopt);

  // Render the release date of the show
  textRenderer.renderTextInRect(modalContent.releaseDate, renderer,
                                modal.releaseDateRect,
                                TextRenderer::TextAdjust::Left, std::nullopt);

  // Create a highlight around the active button within the modal
  const SDL_Rect buttonOutline =
      createHighlightRectangle(modal.getActiveButtonRect());
  SDL_SetRenderDrawColor(renderer, Colors::White);
  SDL_RenderFillRect(renderer, &buttonOutline);

  // Render the buttons within the modal
  SDL_Color button1Color, button2Color = Colors::InActiveButtonColor;
  switch (modal.activeButton) {
  case Modal::ActiveButton::BUTTON1:
    button1Color = Colors::ActiveButtonColor;
    button2Color = Colors::InActiveButtonColor;
    break;
  case Modal::ActiveButton::BUTTON2:
    button1Color = Colors::InActiveButtonColor;
    button2Color = Colors::ActiveButtonColor;
    break;
  }
  const int buttonFontSize = modal.leftButton.h / 3;
  SDL_SetRenderDrawColor(renderer, button1Color);
  SDL_RenderFillRect(renderer, &modal.leftButton);
  textRenderer.renderTextInRect("Play", renderer, modal.leftButton,
                                TextRenderer::TextAdjust::Center,
                                buttonFontSize);
  SDL_SetRenderDrawColor(renderer, button2Color);
  SDL_RenderFillRect(renderer, &modal.rightButton);
  textRenderer.renderTextInRect("Back", renderer, modal.rightButton,
                                TextRenderer::TextAdjust::Center,
                                buttonFontSize);
}

bool startApplication() {
  // Initialize SDL global state
  auto sdlGlobals = SDLGlobals();
  if (!sdlGlobals.status) {
    spdlog::error("Unable to start SDL!");
    return true;
  }

  // Make initial query for the home screen metadata
  auto homeScreenData = httpGetHomescreen();
  if (!homeScreenData) {
    spdlog::error("Unable to get homescreen data!");
    return true;
  }
  auto homeScreenContent = parseHomeScreen(homeScreenData.value());

  const int screenWidth = SCREEN_WIDTH;
  const int screenHeight = SCREEN_HEIGHT;

  // For determining which textures to load
  //  Any tile rectangle that intersects this rectangle will have its associated
  //  tile image loaded into the cache.
  //  Make it larger than the screen to prefetch images off screen
  const SDL_Rect prefetchTileRectangle{-(screenWidth / 2), -(screenHeight / 2),
                                       screenWidth * 2, screenHeight * 2};
  const SDL_Rect screenRectangle{0, 0, screenWidth, screenHeight};

  const auto window = initWindow(screenWidth, screenHeight, AppName);
  const auto renderer = initRenderer(window.get());

  const int tileWidth = (screenHeight * (1.0 / TILE_ASPECT_RATIO)) / 6;
  const int verticalSpan = screenWidth / 30.;
  const int horizonalSpan = screenWidth / 40.;

  Grid grid(20, verticalSpan, tileWidth, horizonalSpan, verticalSpan,
            screenWidth, screenHeight, homeScreenContent.sizePerCollection());
  MediaLoader mediaLoader(grid.getMaximumTilesDisplayed() * 2, renderer.get());
  TextRenderer textRenderer;
  RefLoader refLoader;
  Modal modal(screenWidth, screenHeight);

  bool quit = false;
  ApplicationState applicationState = ApplicationState::InGrid;

  //
  auto lastTime = std::chrono::steady_clock::now();

  // Handle user inputs
  SDL_Event e;
  while (!quit) {
    // Handle events on queue
    while (SDL_PollEvent(&e) != 0) {
      // User requests quit
      if (e.type == SDL_QUIT) {
        quit = true;
      }
      // User presses a key
      else if (e.type == SDL_KEYDOWN) {
        // Select surfaces based on key press
        switch (e.key.keysym.sym) {
        case SDLK_UP:
          spdlog::info("Up {}", grid);
          switch (applicationState) {
          case ApplicationState::InGrid:
            grid.moveUp();
            break;
          case ApplicationState::InModal:
            modal.moveUp();
            break;
          }
          break;

        case SDLK_DOWN:
          spdlog::info("Down {}", grid);
          switch (applicationState) {
          case ApplicationState::InGrid:
            grid.moveDown();
            break;
          case ApplicationState::InModal:
            modal.moveDown();
            break;
          }
          break;

        case SDLK_LEFT:
          spdlog::info("Left {}", grid);
          switch (applicationState) {
          case ApplicationState::InGrid:
            grid.moveLeft();
            break;
          case ApplicationState::InModal:
            modal.moveLeft();
            break;
          }
          break;

        case SDLK_RIGHT:
          spdlog::info("Right {}", grid);
          switch (applicationState) {
          case ApplicationState::InGrid:
            grid.moveRight();
            break;
          case ApplicationState::InModal:
            modal.moveRight();
            break;
          }
          break;
        case SDLK_RETURN:
        case SDLK_RETURN2:
        case SDLK_SPACE:
          spdlog::info("Select");
          switch (applicationState) {
          case ApplicationState::InGrid:
            if (grid.getActiveTile().has_value()) {
              modal.reset();
              applicationState = ApplicationState::InModal;
            }
            break;
          case ApplicationState::InModal:
            if (modal.activeButton == Modal::ActiveButton::BUTTON2) {
              modal.moveRight();
            }
            applicationState = ApplicationState::InGrid;
            break;
          }

          break;
        case SDLK_BACKSPACE:
        case SDLK_DELETE:
        case SDLK_ESCAPE:
          spdlog::info("Exit");
          applicationState = ApplicationState::InGrid;
          modal.reset();
          break;
        default:
          break;
        }
      }
    }

    // Clear screen and draw background color
    SDL_SetRenderDrawColor(renderer.get(), Colors::BackGroundColor);
    SDL_RenderClear(renderer.get());

    // Calculate time step and the number of frames that should have occured
    // since the last time step
    const auto currentTime = std::chrono::steady_clock::now();
    const auto timeStep = std::chrono::duration_cast<std::chrono::milliseconds>(
                              currentTime - lastTime)
                              .count();
    int framesToUpdate =
        std::floor(timeStep / (1.0 / DSSConstants::MaxFPS * 1000));
    if (framesToUpdate > 0) {
      lastTime = currentTime;
    }

    // Update the state of the grid based on the elapsed time
    grid.update(framesToUpdate);

    // Render the tiles of the grid
    renderGrid(grid, homeScreenContent, mediaLoader, renderer.get(),
               textRenderer, prefetchTileRectangle, screenRectangle);

    // Fetch the data and image for the modal
    //  (Even if the modal is not active we still want to fetch the image so
    //  it's in the cache
    //   when the user selects the current tile)
    int activeRow, activeCol;
    std::tie(activeRow, activeCol) = grid.getActiveRowCol();
    auto modalContent = homeScreenContent.getTileContent(activeRow, activeCol);
    auto modalContentTexture =
        mediaLoader.loadTextureFromURL(modalContent.vertTileURL);

    // Render the modal if needed
    if (applicationState == ApplicationState::InModal) {
      renderModal(modal, modalContent, renderer.get(), textRenderer,
                  modalContentTexture.get());
    }

    // Render the frame and send tot eh screen
    SDL_RenderPresent(renderer.get());

    bool haveRefSetsLeftToLoad =
        grid.getRows().size() < homeScreenContent.numTotalCollections();
    bool atEndOfCurrentlyLoadedSets =
        grid.getActiveRow() >=
        static_cast<int>(homeScreenContent.numLoadedCollections() - 2);
    if (haveRefSetsLeftToLoad && atEndOfCurrentlyLoadedSets) {

      // Pop a ref set off the front
      auto refset = homeScreenContent.refsets.front();

      // Use refLoader to make the get request to load the ref set
      refLoader.requestRefSet(refset);

      // Load any refsets that have been retrieved by refLoader
      for (const auto &[refId, contentSet] : refLoader.getContentSets()) {

        homeScreenContent.removeRefSet(refId);
        if (!contentSet) {
          spdlog::error("Failed to load refset {}", refId);
          // TODO: Consider some kind of retry for failed refs
          continue;
        }
        spdlog::info("Loaded refset {}", refId);

        // Add refset to our current set of loaded content sets and
        //  add a new row of tiles to the grid for the set
        homeScreenContent.contentSets.push_back(contentSet.value());
        grid.addRow(contentSet.value().contents.size());
      }
    }
  }

  return false;
}
