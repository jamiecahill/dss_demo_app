#pragma once

#include "spdlog/fmt/ostr.h" // must be included
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <memory>

// Credit to
// https://lethalguitar.wordpress.com/2017/10/27/raii-with-c-libraries-follow-up/
// for the RAII wrappers around the SDL types

template <typename LibraryType> struct Deleter {};

template <typename LibraryType>
using SDLPtr = std::unique_ptr<LibraryType, Deleter<LibraryType>>;

template <> struct Deleter<SDL_Window> {
  void operator()(SDL_Window *ptr) { SDL_DestroyWindow(ptr); }
};

template <> struct Deleter<SDL_Renderer> {
  void operator()(SDL_Renderer *ptr) { SDL_DestroyRenderer(ptr); }
};

template <> struct Deleter<SDL_Texture> {
  void operator()(SDL_Texture *ptr) { SDL_DestroyTexture(ptr); }
};

template <> struct Deleter<TTF_Font> {
  void operator()(TTF_Font *ptr) { TTF_CloseFont(ptr); }
};

template <> struct Deleter<SDL_Surface> {
  void operator()(SDL_Surface *ptr) { SDL_FreeSurface(ptr); }
};

template <typename LibraryType>
using SDLSharedPtr = std::shared_ptr<LibraryType>;

// Creates an std::shared_ptr that wraps an SDL object
// WARNING: All SDLSharedPtrs created that take initial ownership of the
//  underlying SDL object must be created using this method.
//  Unlike unique_ptr the Deleter object cannot be included within the template
//  and must be provded during shared_ptr construction hence the need for this
//  function
template <typename LibraryType>
std::shared_ptr<LibraryType> make_sdl_shared(LibraryType *p) {
  return std::shared_ptr<LibraryType>(p, Deleter<LibraryType>());
}

// Instantiates the SDL global state upon construction
//  and cleans up the SDL global state upon destruction
struct SDLGlobals {
  bool status = true;

  SDLGlobals();

  ~SDLGlobals();
};

// Creates an SDL_Window
SDLPtr<SDL_Window> initWindow(int screenWidth, int screenHeight,
                              const std::string &windowName);
// Creates an an SDL_Renderer
SDLPtr<SDL_Renderer> initRenderer(SDL_Window *window);

// Creates a SDL_Texture in from an image file
SDLSharedPtr<SDL_Texture> loadTextureFromPath(std::string path,
                                              SDL_Renderer *renderer);

// Creates a SDL_Texture from a string serialized image (e.g. the string
// response from an http get request for a jpeg file)
SDLSharedPtr<SDL_Texture> loadTextureFromStringPng(std::string stringPNG,
                                                   SDL_Renderer *renderer);

// Creates a SDL_Surface from an string serialized image (e.g. the string
// response from an http get request for a jpeg file)
SDLSharedPtr<SDL_Surface> loadSurfaceFromStringPng(std::string);

// Creates an SDL_Surface from an image file
SDLSharedPtr<SDL_Surface> loadSurfaceFromPath(std::string);

// Creates an SLD_Texture  from an SDL_Surface
SDLSharedPtr<SDL_Texture> loadTextureFromSurface(SDL_Surface *surface,
                                                 SDL_Renderer *renderer);

// Pretty print for an SDL_Rect object
std::ostream &operator<<(std::ostream &os, const SDL_Rect &r);
