#pragma once

#include "json.hpp"
#include <optional>

std::optional<nlohmann::json> httpGetHomescreen(void);

std::optional<std::string> httpGetImage(const std::string &imageURL);

std::optional<nlohmann::json> httpGetRefSet(const std::string &refId);
