#pragma once

#include <SDL2/SDL.h>
#include <iostream>
#include <optional>
#include <vector>

const int HIGHLIGHT_BORDER_SIZE = 2;

/// Create an SDL_Rect that encompasses the provided rectangle
SDL_Rect createHighlightRectangle(const SDL_Rect &rect,
                                  int borderSize = HIGHLIGHT_BORDER_SIZE);

/// 500w x 281h aspect ratio
const double TILE_ASPECT_RATIO = (281.0 / 500.0);
const double SELECTED_TILE_SIZE_RATIO = 1.2;

enum TileState { SELECTED, NOTSELECTED };

enum RowState { SWIPINGLEFT, SWIPINGRIGHT, STATIONARY };

class Tile {

public:
  /// Construct a tile
  ///  x - x position of upper lefthand corner
  ///  y - y position of upper lefthand corner
  ///  tilewidth - width in pixels of the tile when unselected
  Tile(int x, int y, int tileWidth);

  void update(int frames, int x, int y);

  void select();
  void deselect();
  TileState getState() const;

  /// Returns the current width of the tile
  int width() const;

  /// Returns the maximum width the tile could be when selected
  int maxHeight() const;

  /// Returns the maximum height the tile could be when selected
  int maxWidth() const;

  const SDL_Rect &getRectangle() const;

private:
  const int tileWidth;
  TileState state;
  SDL_Rect rect;

  /// Returns growth/shrink rate of the tile during the selection/deselection
  /// transition in pixels per frame
  int growthRate(int frames) const;
};

/// Compute the number of tiles hidden to the left of the screen
///  based off the provided row offset, activeColumn index, and number of
///  tiles of the row that can displayed on screen at once
int computeNumberOfTilesOffScreen(int offset, int activeCol,
                                  int maxTilesDisplayed);

/// A row of tiles
class Row {
public:
  Row(int x, int y, int tileWidth, int horizontalSpan, int verticalSpan,
      int numTiles, int rowWidth);

  void deselectAllTiles();

  /// Set the active column (and tile) of the row
  ///  relative to the columns on view of the  screen
  /// relativeCol - int between 0 and (maxTilesDisplayed-1)
  ///    where 0 sets the active column to be the left most
  ///    column of the row on screen and (maxTilesDisplayed-1)
  ///    sets the active column to be the right most column of
  ///    the row on screen
  void setActiveColumnFromRelativeCol(int relativeCol);

  /// Get the index of the active column relative to the columns
  ///  on view of the screen. Returns an int between 0 and
  ///  (maxTilesDisplayed-1) where 0 means the active column is the
  ///  leftmost column on screen and (maxTilesDisplayed-1) means
  ///  the active column is the rightmost column on screen
  int getActiveRelativeCol() const;

  /// Returns the bottom y position of the row
  int bottomY() const;

  /// Returns the top y position of the row
  int topY() const { return rowY; };

  /// Returns the height of the row in pixels
  int rowHeight() const;

  /// Update the state of the row forward the specified number of frames
  ///  updating the tiles and their positions
  void update(int newY, int frames);

  /// Set the column/tile left of the currently active tile
  ///  as the new active column/tile
  void moveLeft();

  /// Set the column/tile right of the currently active tile
  ///  as the new active column/tile
  void moveRight();

  /// Return the active tile if the active tile
  ///  is in this row, std::nullopt otherwise
  std::optional<Tile> getActiveTile() const;

  /// Return the active column of the row
  int getActiveCol() const;

  const std::vector<Tile> &getTiles() const;
  std::vector<Tile> &getTiles();

  friend std::ostream &operator<<(std::ostream &os, const Row &r);

private:
  // Initial x,y position of the row
  const int initialX;
  const int initialY;

  // X poiton of the upper left hand corner of the row
  int rowX;
  // Y poiton of the upper left hand corner of the row
  int rowY;

  // Distance between tiles in pixels
  const int horizontalSpan;

  // Distance between rows in pixels
  const int verticalSpan;

  // Width of a tile in pixels
  const int tileWidth;

  // Total number of columns in the row
  const int numTiles;

  // Max nmumber of columns that can displayed on the screen
  int maxTilesDisplayed;

  std::vector<Tile> tiles;

  // How many tiles of the row are hidden to the left of the screen
  int offset;

  // Left most column in the row displayed
  int activeCol;

  // Expected location of the row after performing any
  // transitions or animations
  int expectedX;

  // Width of the visible portion of the row
  const int rowWidth;
};

class Grid {
public:
  Grid(int x, int y, int tileWidth, int horizontalSpan, int verticalSpan,
       int gridWidth, int gridHeight, std::vector<int> sizePerRow);

  /// Returns the height in pixels of a row within the grid
  int rowHeight();

  /// Move the cursor left by one tile
  void moveLeft();

  /// Move the cursor right by one tile
  void moveRight();

  /// Deselect all active tiles
  void deSelectAllTiles();

  /// Move the cursor up one row of tiles
  void moveUp();

  /// Move the cursor up one row of tiles
  void moveDown();

  /// Move the grid forward in time updating the elements of the grid to reflect
  /// the current state
  ///  of the grid and playing any animations
  void update(int frames);

  /// Return the active tile if there is an active tile within the grid,
  /// std::nullopt otherwise
  std::optional<Tile> getActiveTile() const;

  const std::vector<Row> &getRows() const;

  friend std::ostream &operator<<(std::ostream &os, const Grid &grid);

  /// Returns the maximum number of tiles from the grid that could be displayed
  /// on the screen at once (including partially displayed tiles)
  int getMaximumTilesDisplayed();

  int getActiveRow() const { return activeRow; }

  void addRow(int numtiles);

  SDL_Rect getRowTitleRect(int row) const;

  std::pair<int, int> getActiveRowCol() {
    return std::make_pair(getActiveRow(), rows[getActiveRow()].getActiveCol());
  }

private:
  // Row objects for each row of tiles within the grid
  std::vector<Row> rows;

  // Index of the currently selected row
  int activeRow;

  // Number of rows hidden above the screen
  int offset;

  // Current y position of the grid
  int currentY;

  // Initial x posibon at which the grid was created
  const int initialX;

  // Initial y positon at which the grid was created
  const int initialY;

  // Maximum number of rows fully in view on the screen at a time
  int maxRowsDisplayed;

  // Horizontal space in pixels between tiles
  const int horizontalSpan;

  // Vertical space in pixels between rows
  const int verticalSpan;

  // Expected y position of the grid after any transitions/animations occur
  int expectedY;

  // Width of a tile in pixels
  const int tileWidth;

  // Width and height of the visible portion grid of a tile in pixels
  const int gridWidth;
  const int gridHeight;
};
