#pragma once

#include "Endpoints.h"
#include "SDLWrapper.h"
#include "json.hpp"

struct RefSet {

  RefSet(std::string refId, std::string title) : refId(refId), title(title) {}

  // "/refId"
  std::string refId;

  // "/text/title/full/set/default/content"
  std::string title;
};

struct Content {
  Content(std::string contentId, std::string title, std::string releaseDate,
          std::string rating, std::string tileURL, std::string vertTileURL)
      : contentId(contentId), title(title), releaseDate(releaseDate),
        rating(rating), tileURL(tileURL), vertTileURL(vertTileURL) {}
  // "/contentId"
  std::string contentId;

  // "/text/title/full/series/default/content"
  std::string title;

  // "/releases/0/releaseDate"
  std::string releaseDate = "";

  // "/ratings/0/value"
  std::string rating = "";

  // "/image/tile/1.78/series/default/url"
  std::string tileURL;

  // "/image/tile/0.71/series/default/url"
  std::string vertTileURL;
};

struct ContentSet {
  ContentSet(std::string setId, std::string title)
      : setId(setId), title(title) {}
  std::string setId;
  std::string title;
  std::vector<Content> contents;

  int size() const { return contents.size(); }
};

class HomeScreenContent {
public:
  // Return the Content for the specified row and column
  const Content &getTileContent(int row, int column) const {
    return contentSets[row].contents[column];
  }

  // Get the title to display for the specified row
  const std::string &getCollectionTitle(int row) const {
    return contentSets[row].title;
  }

  // Returns the total number of content collections (both loaded and not
  // loaded)
  size_t numTotalCollections() const {
    return contentSets.size() + refsets.size();
  }
  // Returns the number of contents sets current loaded
  size_t numLoadedCollections() const { return contentSets.size(); }

  // Returns the number size of each content collection
  std::vector<int> sizePerCollection() const;

  // Removes the specifed refset
  void removeRefSet(std::string refId);

  // Loaded conent sets
  std::vector<ContentSet> contentSets;

  // Content sets yet to be loaded
  std::vector<RefSet> refsets;
};

// Parses the inital JSON payload
HomeScreenContent parseHomeScreen(const nlohmann::json &homescreendata);

// Parse a JSON conent set (both from the initial payload and ref sets)
// Can be any one of {"PersonalizedCuratedSet", "CuratedSet", "set",
// "TrendingSet"}
ContentSet parseContentSet(const nlohmann::json &container);
