#pragma once
#include "SDLWrapper.h"

bool startApplication();

enum ApplicationState { InGrid, InModal };

SDLPtr<SDL_Texture> loadTextureFromURL(std::string url, SDL_Renderer *renderer);
