#include "JSONReader.h"
#include "spdlog/spdlog.h"
#include <fstream>
#include <iomanip> // std::setw
#include <iostream>

using json = nlohmann::json;

HomeScreenContent parseHomeScreen(const json &homescreendata) {
  HomeScreenContent homeScreenContent;
  for (const auto &container :
       homescreendata["data"]["StandardCollection"]["containers"]) {
    if (container.contains("set") && container["set"].contains("items")) {
      homeScreenContent.contentSets.emplace_back(parseContentSet(container));
    } else if (container.contains("set") &&
               container["set"].contains("refId")) {
      auto refId = container["set"]["refId"];
      auto title = container["set"]["text"]["title"]["full"]["set"]["default"]
                            ["content"];
      homeScreenContent.refsets.emplace_back(RefSet(refId, title));
    }
  }
  return homeScreenContent;
}

std::string getReleaseDate(const json &j) {
  auto releaseDate = j["/releases/0/releaseDate"] != nullptr
                         ? j["/releases/0/releaseDate"]
                         : j["/releases/0/releaseYear"];
  if (releaseDate.is_string())
    return releaseDate;
  return std::to_string(releaseDate.get<int>());
}

ContentSet parseContentSet(const json &container) {
  std::string key = "";
  for (auto &possiblekey :
       {"PersonalizedCuratedSet", "CuratedSet", "set", "TrendingSet"}) {
    if (container.contains(possiblekey)) {
      key = possiblekey;
      break;
    }
  }
  if (key == "") {
    spdlog::error("Unable to parse content set!");
  }
  auto setId = container[key]["setId"];
  auto setTitle =
      container[key]["text"]["title"]["full"]["set"]["default"]["content"];
  ContentSet contentSet(setId, setTitle);
  for (const auto &item : container[key]["items"]) {
    auto j = item.flatten();
    try {
      if (j.contains("/text/title/full/series/default/content")) {
        auto contentId = j["/contentId"];
        auto title = j["/text/title/full/series/default/content"];
        auto releaseDate = getReleaseDate(j);
        auto rating = j["/ratings/0/value"];
        auto tileURL = j["/image/tile/1.78/series/default/url"];
        auto vertTileURL = j["/image/tile/0.71/series/default/url"];
        contentSet.contents.emplace_back(Content(contentId, title, releaseDate,
                                                 rating, tileURL, vertTileURL));
      } else if (j.contains("/text/title/full/program/default/content")) {
        auto contentId = j["/contentId"];
        auto title = j["/text/title/full/program/default/content"];
        auto releaseDate = getReleaseDate(j);
        auto rating = j["/ratings/0/value"];
        auto tileURL = j["/image/tile/1.78/program/default/url"];
        auto vertTileURL = j["/image/tile/0.71/program/default/url"];
        contentSet.contents.emplace_back(Content(contentId, title, releaseDate,
                                                 rating, tileURL, vertTileURL));
      } else if (j.contains("/collectionGroup/contentClass")) {
        auto contentId =
            j.contains("/contentId") ? j["/contentId"] : j["/collectionId"];
        auto title = j["/text/title/full/collection/default/content"];
        auto releaseDate = "";
        auto rating = "";
        auto tileURL = j.contains("/image/tile/1.78/default/default/url")
                           ? j["/image/tile/1.78/default/default/url"]
                           : j["/image/tile/1.78/series/default/url"];
        auto vertTileURL = j.contains("/image/tile/0.71/default/default/url")
                               ? j["/image/tile/0.71/default/default/url"]
                               : j["/image/tile/0.71/series/default/url"];
        contentSet.contents.emplace_back(Content(contentId, title, releaseDate,
                                                 rating, tileURL, vertTileURL));
      }
    } catch (json::exception &e) {
      spdlog::error("Unable to parse JSON content : {}", e.what());
      std::string tmpname = "failed.json";
      std::ofstream out(tmpname);
      out << j;
      out.close();
    }
  }
  return contentSet;
}

std::vector<int> HomeScreenContent ::sizePerCollection() const {
  std::vector<int> output;
  for (auto &contentSet : contentSets) {
    output.push_back(contentSet.size());
  }
  return output;
}

void HomeScreenContent ::removeRefSet(std::string refId) {
  size_t idx = 0;
  // Find the ref set to delete by id
  for (idx = 0; idx < refsets.size(); idx++) {
    if (refsets[idx].refId == refId) {
      break;
    }
  }
  // If the ref set was found remove it
  if (idx < refsets.size()) {
    refsets.erase(refsets.begin() + idx);
  }
}
