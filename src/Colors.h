#pragma once
#include <SDL2/SDL.h>

namespace Colors {
const SDL_Color BackGroundColor = {54, 57, 63, 255};
const SDL_Color White = {255, 255, 255, 255};
const SDL_Color ModalBackgroundColor = {32, 34, 37, 255};
const SDL_Color ActiveButtonColor = {192, 190, 103, 255};
const SDL_Color InActiveButtonColor = {116, 127, 141, 255};
const SDL_Color BlankTileColor = {48, 51, 57, 255};
}; // namespace Colors
