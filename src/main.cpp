#include "DSSApp.h"

int main(int argc __attribute__((unused)),
         char *args[] __attribute__((unused))) {
  bool error = startApplication();
  if (error) {
    return 1;
  }
  return 0;
}
