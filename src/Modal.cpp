#include "Modal.h"

Modal::Modal(int screenWidth, int screenHeight) {
  // Create a rectangle in the middle of the screen for the modal
  modalRect = {static_cast<int>(screenWidth / 7.),
               static_cast<int>(screenHeight / 5.),
               static_cast<int>(screenWidth * 4. / 5.),
               static_cast<int>(screenHeight * 2. / 4.)};

  const int vertTileHeight = static_cast<int>(modalRect.h * (4. / 5.));
  const int vertTileWidth =
      static_cast<int>(VERT_CONTENT_TILE_ASPECT_RATIO * vertTileHeight);
  vertContentTile = {static_cast<int>(modalRect.x + modalRect.w * 1. / 10.),
                     static_cast<int>(modalRect.y + modalRect.h * 1. / 10.),
                     vertTileWidth, vertTileHeight};
  const int descriptionColX =
      vertContentTile.x +
      static_cast<int>(modalRect.x + modalRect.w * 2. / 10.);
  const int descriptionColWidth =
      modalRect.w - (vertContentTile.x +
                     static_cast<int>(modalRect.x + modalRect.w * 1. / 10.));
  titleRect = {descriptionColX, vertContentTile.y, descriptionColWidth,
               static_cast<int>(vertContentTile.h * 1. / 5.)};

  // Cheating a bit here. Rating is left adjusted while release will be right
  // adjusted but
  //  they occupy the same line and have the same height
  ratingRect = {descriptionColX, titleRect.y + titleRect.h + (titleRect.h / 2),
                titleRect.w, titleRect.h / 2};
  releaseDateRect = ratingRect;

  const int buttonY = vertContentTile.y + (vertContentTile.h / 2);
  const int buttonGap = static_cast<int>(titleRect.w * 1. / 9.);
  const int buttonWidth = static_cast<int>(titleRect.w * 4. / 9.);
  leftButton = {descriptionColX, buttonY, buttonWidth, vertContentTile.h / 2};
  rightButton = {leftButton.x + buttonWidth + buttonGap, buttonY, buttonWidth,
                 vertContentTile.h / 2};
  reset();
}
