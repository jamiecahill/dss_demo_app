#pragma once
#include "Endpoints.h"
#include "JSONReader.h"
#include "LRUCache.h"
#include "SDLWrapper.h"
#include <condition_variable> // std::condition_variable
#include <deque>              // std::queue
#include <mutex>
#include <set> // std::queue
#include <thread>

// Handles loading media conent like tile images
// To avoid blocking the main loop http get requests and loading the response
//  into an SDL surface occurs in a worker thread. Rendering still
//  must occur in the main thead due to SDL restrictions.
// An LRU cache is used to store the textures to avoid repeat work
//  rendering each frame. It is recommended that the minimum cache size be
//  the maximum number of unique tiles being displayed in the screen at once
//  Improvements:
//  - Use a pool of workers
//  - Add signal to wake worker thread
class MediaLoader {
public:
  MediaLoader(int cacheSize, SDL_Renderer *renderer)
      : cache(cacheSize), renderer(renderer), runWorkerThread(true) {
    // Kick off the thread to perform the HTTP get requests
    workerThread = std::thread(&MediaLoader::loadDataInThread, this);
  };

  // Returns the texture for the specified image http endpoint
  //  if the texture for the endpoint in in the cache
  //  If the image is not in the cache a request is made to the worker
  //  thread to get the image and a blank texture is returned instead
  //  TODO: Consider adding an optional height and width inputs. The endpoints
  //  support requesting a specific image
  //   size which is not currently being utilitzed
  SDLSharedPtr<SDL_Texture> loadTextureFromURL(std::string url) {
    loadResponses();
    const std::lock_guard<std::mutex> lock(dataLock);
    if (cache.exists(url)) {
      return cache.get(url);
    }
    if (urlRequestSet.find(url) == urlRequestSet.end()) {
      urlRequestQueue.push_back(url);
      urlRequestSet.insert(url);
      // Tell the worker to wakeup
      workerConditionVariable.notify_one();
    }
    return SDLSharedPtr<SDL_Texture>(nullptr);
  }

  // On deletion, signal the worker thread to stop and wait for it to finish
  ~MediaLoader() {
    runWorkerThread = false;
    // Wake the worker so it exits from it's loop
    workerConditionVariable.notify_one();
    workerThread.join();
  }

private:
  // Intended to be run by the main thread, loop through all pending
  //  SDL_Surfaces loaded by the worker thread, load them into SDL_Textures,
  //  and store in the cache
  void loadResponses() {
    const std::lock_guard<std::mutex> lock(dataLock);
    for (auto &response : urlResponseQueue) {
      if (!cache.exists(response.first)) {
        auto texture = loadTextureFromSurface(response.second.get(), renderer);
        if (texture == nullptr) {
          spdlog::error("Failed to loead surface from url {}", response.first);
        }
        cache.put(response.first, texture);
      }
    }
    urlResponseQueue.clear();
  }

  // Function intended to be run by the worker thread. Spins in a loop
  //  dequeueing elements from urlRequestQueue, performing the http get request
  //  and loading associated response into an SLD_Surface, and enqueueing the
  //  surface into urlResponseQueue for the main thread to consume
  void loadDataInThread() {
    while (runWorkerThread) {

      // If nothing is in the queue wait till the main thread wakes the worker
      //  to avoid spending cycles polling the size of the queue
      std::unique_lock<std::mutex> loopLock(workerWakeLock);
      workerConditionVariable.wait(
          loopLock, [&] { return urlRequestQueue.size() || !runWorkerThread; });
      std::string url = "";
      // Check to see if the main thread has requsted any images
      if (urlRequestQueue.size()) {
        const std::lock_guard<std::mutex> lock(dataLock);
        // Pop off the first element of the queue
        url = urlRequestQueue.front();
        urlRequestQueue.pop_front();
        urlRequestSet.erase(urlRequestSet.find(url));
      }
      // If there is a texture to loead
      if (url != "") {
        auto stringPng = httpGetImage(url);
        // If the get request failed send a blank SDL_Surface back to the main
        // thread
        //  for the requested url
        if (!stringPng) {
          spdlog::error("Unable to load image from url {}", url);
          const std::lock_guard<std::mutex> lock(dataLock);
          urlResponseQueue.push_back(
              std::make_pair(url, SDLSharedPtr<SDL_Surface>(nullptr)));
        }
        // If the get request succeeded load the response from the get request
        // into
        //  and SDL_Surface and send back to the main thread
        else {
          auto surface = loadSurfaceFromStringPng(stringPng.value());
          const std::lock_guard<std::mutex> lock(dataLock);
          urlResponseQueue.push_back(std::make_pair(url, surface));
        }
      }
    }
  }

  // For sending requests to the worker thread. The main thread inserts
  //  url endpoints to query and the woker thread consumes elements from this
  //  queue. We use the queue to store the urls requested in order
  //  and use the set to keep track of which urls are already in the queue
  //  as the deque data structure does not support fast finds
  std::set<std::string> urlRequestSet;
  std::deque<std::string> urlRequestQueue;

  // For sending responses from the worker thread to the main thread.
  //  The worker thread enques SDL_Surfaces loaded from the url endpoint
  //  and the main thead consumes the elements from this queue
  std::deque<std::pair<std::string, SDLSharedPtr<SDL_Surface>>>
      urlResponseQueue;

  // Lock for coordinating between the worker thread and main thread. Any access
  // to
  //  the shared queues above must coordinated with this lock
  std::mutex dataLock;
  // Lock for sending a signal to wake the worker thread
  //  We use this lock to let the worker sleep and singal it to wake
  //   up so it's not spending time spinning in a loop polling
  //   when there aren't requests to fecth
  std::mutex workerWakeLock;
  std::condition_variable workerConditionVariable;

  // Cache storing the most recently requested textures
  cache::lru_cache<std::string, SDLSharedPtr<SDL_Texture>> cache;

  std::thread workerThread;

  // SLD_Renderer used to load the SDL_Surfaces into textures
  // TODO: Consider making a shared_ptr resource
  SDL_Renderer *renderer;

  // While true the woker thread will spin loading respones. When set to false
  //  the woker thread will exit its loop and terminate processing.
  bool runWorkerThread = true;
};
