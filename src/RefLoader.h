#pragma once
#include "JSONReader.h"
#include "spdlog/spdlog.h"
#include <chrono>
#include <future>
#include <set>

template <typename R> bool is_ready(std::future<R> const &f) {
  return f.wait_for(std::chrono::seconds(0)) == std::future_status::ready;
}

class RefLoader {
public:
  RefLoader() = default;

  void requestRefSet(const RefSet &refset) {
    if (pendingRefRequests.find(refset.refId) == pendingRefRequests.end()) {

      spdlog::info("Loading refset {}", refset.refId);
      pendingRefRequests.insert(std::make_pair(
          refset.refId, std::async(std::launch::async, &RefLoader::loadRefSet,
                                   this, refset)));
    }
  }

  std::map<std::string, std::optional<ContentSet>> getContentSets() {
    std::map<std::string, std::optional<ContentSet>> loadedContent;

    for (auto &[refId, future] : pendingRefRequests) {
      if (is_ready(future)) {
        loadedContent[refId] = future.get();
      }
    }
    for (auto const &[refId, _] : loadedContent) {
      pendingRefRequests.erase(pendingRefRequests.find(refId));
    }
    return loadedContent;
  }

  int size() const { return pendingRefRequests.size(); }

private:
  std::optional<ContentSet> loadRefSet(const RefSet &refset) {
    auto refsetresponse = httpGetRefSet(refset.refId);
    if (!refsetresponse) {
      spdlog::error("Failed to load refset {}!", refset.refId);
      return std::nullopt;
    }
    return parseContentSet(refsetresponse.value()["data"]);
  }

  std::map<std::string, std::future<std::optional<ContentSet>>>
      pendingRefRequests;
};
